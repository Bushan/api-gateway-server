# Zuul Server

This is API Gateway service which identifies services from discovery server 'Eureka' 

## Running

Running from CLI
 
	$ java -jar api-gateway-server-0.0.1.jar

or, Running main method class 'ApiGatewayServerApplication.java' from IDE or CLI

Its configured to run on port '9090' 

Running in Docker container

	$ docker run -p 9090:9090 -e "EUREKA_SERVER_HOST=<server IP>" -e "EUREKA_SERVER_PORT=<server port e.g. 8761>" -t <image-prefix>/api-gateway-server:<image-tag>
	 
URL for discovered service 
	
	http://<server>:9090/api/<service-registered-name-eureka>/<end-point>
	e.g. : 'http://localhost:9090/api/department-service/departments' 
